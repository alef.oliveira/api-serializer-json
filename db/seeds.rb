# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Address.delete_all
Phone.delete_all
Person.delete_all

1..100.times do |person|
  Person.create!(
    name: FFaker::Name.name,
    age: Random.rand(1..100)
  )
  puts "Created person #{person}"
end

Person.all.each do |person|
  Phone.create(
    person: person,
    number: FFaker::PhoneNumberBR.mobile_phone_number.to_s
  )
  # person.phone.create!(number: FFaker::PhoneNumberBR.mobile_phone_number.to_s)
  puts "Created phone fro #{person.name}"
end

Person.all.each do |person|
  1..[1, 2, 3].sample.times do |_address|
    Address.create(
      person: person,
      number: FFaker::AddressBR.building_number,
      street: FFaker::AddressBR.street,
      neighborhood: FFaker::AddressBR.neighborhood,
      city: FFaker::AddressBR.city,
      zip_code: FFaker::AddressBR.zip_code
    )
  end
  puts "Created #{person.addresses.count} addresses for #{person.name}"
end
