class Person < ApplicationRecord
  validates :name, :age, presence: true
  has_one :phone
  has_many :addresses
end
