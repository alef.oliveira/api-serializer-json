class Address < ApplicationRecord
  belongs_to :person
  validates :number, :street, :neighborhood, :city, :zip_code, presence: true
end
