class PersonSerializer < ActiveModel::Serializer
  attributes :id, :name, :age
  has_one :phone
  has_many :addresses

  meta do
    { nationality: 'Brazilian' }
  end
end
