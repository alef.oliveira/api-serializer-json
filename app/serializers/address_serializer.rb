class AddressSerializer < ActiveModel::Serializer
  attributes :id, :street, :number, :neighborhood, :city, :zip_code
  belongs_to :person
end
